import './index.scss';
import GSheetReader from 'g-sheets-api';
import { createEl, addEl } from 'lmnt';
import onChange from 'on-change';
import autoBind from 'auto-bind';
import { shuffle } from './modules/utils';

const apiKey = 'AIzaSyDtVWjuGmlXpJmiKibSpLxlf0vkKFqGCsU';
const sheetId = '1dSqqlukLzamnB3G9_LBtRQIPdZYl28SKRB0qoq2Dvyg';
const sheetName = 'Questions';
const options = {
  apiKey,
  sheetId,
  sheetName,
};

class App {
  constructor() {
    autoBind(this);
    const state = {
      grades: [],
      subjects: [],
      quetions: [],
      maxQuestions: 0,
    };

    this.grades = [];
    this.subjects = [];
    this.randomQuestionOrder = [];
    this.randomAnswerOrder = [];

    this.state = onChange(state, this.update, { ignoreKeys: ['questions'] });

    this.el = createEl('div', { className: 'app' });
    // this.title = createEl('h1', { className: 'title', innerText: 'MathRadio Quiz Generator' });

    // addEl(this.el, this.title);
    this.getQuestions().then(this.setupUI);
  }

  getQuestions() {
    return new Promise((resolve) => {
      GSheetReader(
        options,
        (data) => {
          this.questions = data;
          this.questions.forEach((question, index) => {
            question.index = index + 2;
            // TODO: these can be .map's or .reduce's
            question.grade.split('|').forEach((grade) => {
              if (this.grades.indexOf(grade) === -1) this.grades.push(grade);
            });
            question.subject.split('|').forEach((subject) => {
              if (this.subjects.indexOf(subject) === -1) this.subjects.push(subject);
            });
          });

          this.grades.sort();
          // this.questions

          this.questions.forEach((question) => {
            question.choices = question.choices.split('|');
            question.grades = question.grade.split('|').map(item => parseInt(item, 10));
            question.subjects = question.subject.split('|');
            delete question.grade;
            delete question.subject;
          });

          // TODO: implement this and pull into separate methods so user can call them later
          // this.randomQuestionOrder = [];
          // for (let i = 0; i < this.questions.length; i += 1) {
          //   // this.randomQuestionOrder.push()
          // }
          this.randomizeQuestions();

          this.randomAnswerOrder = [];
          for (let i = 0; i < this.questions.length; i += 1) {
            for (let j = 0; j < this.questions[i].choices.length; j += 1) {
              // this.randomAnswerOrder.push()
            }
          }

          return resolve();
        },
      );
    });
  }

  shuffleQuestions() {
    this.randomizeQuestions();
    this.displayQuestions();
  }

  randomizeQuestions() {
    const order = [];
    for (let i = 0; i < this.questions.length; i += 1) {
      order.push(i);
    }
    this.randomQuestionOrder = shuffle(order);
  }

  randomizeAnswers() {

  }

  setupUI() {
    this.options = createEl('div', { className: 'options' });
    // this.header = createEl('div', { className: 'header' });
    this.quiz = createEl('div', { className: 'quiz' });

    this.gradeSelect = createEl('checkbox', { className: 'options-grade', name: 'Grade Level', values: this.grades }, {}, {
      changed: ({ detail }) => { this.state.grades = detail.map(grade => parseInt(grade, 10)); },
    });

    this.subjectSelect = createEl('checkbox', { className: 'options-subject', name: 'Subject', values: this.subjects }, {}, {
      changed: ({ detail }) => { this.state.subjects = detail; },
    });

    this.questionAmount = createEl('div', { className: 'options-amount' });
    this.questionAmountLabel = createEl('p', { innerText: 'there are no matching questions' });
    this.amountSelect = createEl('range', { className: 'options-amount-select hidden', name: 'Maximum Questions', min: 1, max: this.questions.length, value: this.questions.length }, {}, {
      input: ({ target }) => { this.state.maxQuestions = parseInt(target.value, 10); },
    });
    addEl(this.questionAmount, this.questionAmountLabel, this.amountSelect);

    this.shuffleQuestionsButton = createEl('button', { className: 'shuffle-questions-button', innerText: 'Shuffle Question Order' }, {}, {
      click: this.shuffleQuestions,
    });

    this.shuffleAnswersButton = createEl('button', { className: 'shuffle-answers-button', innerText: 'Shuffle Answers Orer' }, {}, {
      click: () => { console.log('change '); },
    });
    this.printButton = createEl('button', { className: 'print-button', innerText: 'Print Quiz' }, {}, { click: this.printQuiz });

    addEl(this.options, this.gradeSelect, this.subjectSelect, this.questionAmount, this.shuffleQuestionsButton, this.printButton);
    addEl(this.el, this.options, this.quiz);
    // this.state.maxQuestions = this.questions.length;
  }

  update(path, current, previous) {
    console.log(path, previous, '->', current);
    if (path === 'maxQuestions') {
      // this.amountSelect.querySelector('input').max = this.state.maxQuestions;
      // this.amountSelect.querySelector('input').value = this.state.maxQuestions;
      // this.amountSelect.querySelector('p').innerText = this.state.maxQuestions;
      return this.displayQuestions();
    }

    const availableQuestions1 = this.questions.filter(question => (this.state.grades.length && this.state.grades.some(g => question.grades.includes(g))));
    const availableQuestions2 = this.questions.filter(question => (this.state.subjects.length && this.state.subjects.some(s => question.subjects.includes(s))));

    // if at least one grade and one subject is selected, use an exclusive filter
    if (this.state.grades.length && this.state.subjects.length) {
      this.state.questions = availableQuestions1.filter(question => availableQuestions2.includes(question));
    }
    // if no grade or no subject selected, use an inclusive filter
    else {
      this.state.questions = availableQuestions1.concat(availableQuestions2);
    }

    this.questionAmount = this.state.questions.length;
    if (this.questionAmount !== this.questionAmountPrevious) {
      console.log('question amount changed');
    }
    this.questionAmountPrevious = this.questionAmount;

    this.state.maxQuestions = this.state.questions.length;
    this.amountSelect.querySelector('input').max = this.state.maxQuestions;
    this.amountSelect.querySelector('input').value = this.state.maxQuestions;
    this.amountSelect.querySelector('p').innerText = this.state.maxQuestions;


    this.amountSelect.classList[this.state.maxQuestions > 0 ? 'remove' : 'add']('hidden');
    this.displayQuestions();
  }

  displayQuestions() {
    this.quiz.innerHTML = '';
    // const questions = shuffle(this.state.questions);
    let count = 0;
    for (let i = 0; i < this.questions.length; i += 1) {
      // this.randomQuestionOrder[i])
      const question = this.state.questions[this.randomQuestionOrder[i]];
      if (question) {
        count += 1;
        if (count <= this.state.maxQuestions) {
          const questionEl = createEl('div', { className: 'quiz-question' });
          const number = createEl('p', { className: 'quiz-question-number' });
          const text = createEl('h1', { className: 'quiz-question-text', innerText: question.question });
          const answersEl = createEl('ol', { className: 'quiz-question-answers' });
          // const answers = shuffle(question.choices);
          const answers = question.choices;
          answers.forEach((answer) => {
            const answerEl = createEl('li', { className: 'quiz-question-aswers', innerText: answer });
            addEl(answersEl, answerEl);
          });
          const subjectContainer = createEl('div', { className: 'quiz-question-subjects' });
          question.subjects.forEach((subject) => {
            const subjectEl = createEl('span', { className: 'quiz-question-subjects-subject', innerText: subject });
            addEl(subjectContainer, subjectEl);
          });

          addEl(questionEl, number, subjectContainer, text, answersEl);
          addEl(this.quiz, questionEl);
          question.el = questionEl;
        }
      }
    }

    this.questionAmountLabel.innerText = `there are ${this.state.questions.length || 'no'} matching questions`;
  }

  hideHeader() {
    // this.title.style.display = 'none';
    this.options.style.display = 'none';
  }

  showHeader() {
    // this.title.style.display = 'block';
    this.options.style.display = 'block';
    window.removeEventListener('afterprint', this.showHeader);
  }

  printQuiz() {
    this.hideHeader();
    window.addEventListener('afterprint', this.showHeader);
    window.print();
  }
}


const app = new App();
addEl(app.el);
if (location.hostname === 'localhost') window.app = app;
