export const shuffle = (array) => {
  let currentIndex = array.length;
  let temporaryValue; let
    randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};

export const containsAdjacents = (array, columns) => {
  for (let i = 0; i < array.length - 1; i += 1) {
    // if this is not the last item in the row and it's adjacent to an item of the same value return true
    if ([i + 1] % columns > 0 && array[i] === array[i + 1]) return true;
  }
  return false;
};
